<!--
*** Thanks for checking out this README Template. If you have a suggestion that would
*** make this better, please fork the repo and create a pull request or simply open
*** an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
***
***
***
*** To avoid retyping too much info. Do a search and replace for the following:
*** github_username, repo_name, twitter_handle, email
-->





<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<!-- PROJECT LOGO -->
<!-- <br />
<p align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="images/logo.png" alt="Logo" width="80" height="80">
  </a> -->

# Assignment 1 - RPGCharacter


<!-- ABOUT THE PROJECT -->
## About The Project

This project was created as an assignment.
<br />
Characters can equip weapons and armor. They can also level up.
<br />
Each level gives the character better stats, so they can do more damage.
<br />
Weapons and armor give characters stat boosts.

## RPG Character
The RPG Character is created with:
- Name
- Character Type (Warrior, Rogue, Ranger, Mage)
- Level
- Primary Attributes (Strength, Dexterity, Intelligence)
- Equipment Slots (Head, Body, Legs, Weapon)

## Weapon
A weapon is created with:
- Name
- Weapon Level
- Weapon Type
- Weapon Slot
- Weapon Attributes (Damage, Attack speed)

## Armor
Armor is created with:
- Name
- Armor Level
- Armor Type
- Armor Slot
- Attributes (Strength, Dexterity, Intelligence)

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Installation

1. Clone the repo
```sh
git clone https://gitlab.com/ThymenH/assignment1_rpgcharacter.git
```

<!-- USAGE EXAMPLES -->
## Usage

A few testcases have been added in the program.cs file.
<br />
Unit tests have been added to the project.

