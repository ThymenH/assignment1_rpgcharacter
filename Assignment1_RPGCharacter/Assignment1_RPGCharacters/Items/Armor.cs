﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public class Armor : Item
    {
        public ArmorType TypeArmor { get; set; }
        public PrimaryAttributes Attributes { get; set; }

        public Dictionary<ArmorType, Item> ArmorType = new Dictionary<ArmorType, Item>();
    }
}
