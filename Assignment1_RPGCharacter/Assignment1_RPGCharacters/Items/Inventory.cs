﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public class Inventory
    {
        public Dictionary<InventorySlots,Weapon> WeaponContents = new Dictionary<InventorySlots, Weapon>();
        public Dictionary<InventorySlots, Armor> ArmorContents = new Dictionary<InventorySlots, Armor>();

    }
}
