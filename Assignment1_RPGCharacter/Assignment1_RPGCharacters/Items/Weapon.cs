﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public class Weapon : Item
    {
        public WeaponType TypeWeapon { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

        public Dictionary<WeaponType, Item> WeaponType = new Dictionary<WeaponType, Item>();
    }
}
