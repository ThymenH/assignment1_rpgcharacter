﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public enum InventorySlots
    {
        SLOT_WEAPON,
        SLOT_HEAD,
        SLOT_BODY,
        SLOT_LEGS
    }
}
