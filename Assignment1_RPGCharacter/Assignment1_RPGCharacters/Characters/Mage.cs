using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public class Mage : Character
    {
        /// <summary>
        /// Creates mage
        /// </summary>
        public void CreateMage()
        {
            Mage baseLevel = new Mage()
            {
                Name = "Jason",
                Level = 1,
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8,
                CharacterType = CharacterType.CHARACTER_MAGE
            };

            CharacterBase(baseLevel.Name, baseLevel.Level, baseLevel.Strength, baseLevel.Dexterity, baseLevel.Intelligence, baseLevel.CharacterType);

            AllowedWeapons.Add(WeaponType.WEAPON_STAFF);
            AllowedWeapons.Add(WeaponType.WEAPON_WAND);
            AllowedArmor.Add(ArmorType.ARMOR_CLOTH);
        }

        /// <summary>
        /// Levels up mage
        /// </summary>
        public void LevelUpMage()
        {
            Mage levelUpMage = new Mage()
            {
                Strength = Strength + 1,
                Dexterity = Dexterity + 1,
                Intelligence = Intelligence + 5
            };

            CurrentStats(levelUpMage.Strength, levelUpMage.Dexterity, levelUpMage.Intelligence);

            levelUp();
        }
    }
}
