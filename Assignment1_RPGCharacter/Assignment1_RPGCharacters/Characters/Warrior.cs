using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public class Warrior : Character
    {
        /// <summary>
        /// Creates warrior
        /// </summary>
        public void CreateWarrior()
        {
            Warrior baseLevel = new Warrior()
            {
                Name = "John",
                Level = 1,
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1,
                CharacterType = CharacterType.CHARACTER_WARRIOR
            };

            CharacterBase(baseLevel.Name, baseLevel.Level, baseLevel.Strength, baseLevel.Dexterity, baseLevel.Intelligence, baseLevel.CharacterType);

            AllowedWeapons.Add(WeaponType.WEAPON_AXE);
            AllowedWeapons.Add(WeaponType.WEAPON_HAMMER);
            AllowedWeapons.Add(WeaponType.WEAPON_SWORD);
            AllowedArmor.Add(ArmorType.ARMOR_MAIL);
            AllowedArmor.Add(ArmorType.ARMOR_PLATE);
        }

        /// <summary>
        /// Levels up warrior
        /// </summary>
        public void LevelUpWarrior()
        {
            Warrior levelUpWarrior = new Warrior()
            {
                Strength = Strength + 3,
                Dexterity = Dexterity + 2,
                Intelligence = Intelligence + 1
            };

            CurrentStats(levelUpWarrior.Strength, levelUpWarrior.Dexterity, levelUpWarrior.Intelligence);

            levelUp();      
        }
    }
}
