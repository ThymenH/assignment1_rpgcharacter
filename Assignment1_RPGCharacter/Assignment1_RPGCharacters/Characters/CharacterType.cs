﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public enum CharacterType
    {
        CHARACTER_MAGE,
        CHARACTER_RANGER,
        CHARACTER_ROGUE,
        CHARACTER_WARRIOR
    }
}
