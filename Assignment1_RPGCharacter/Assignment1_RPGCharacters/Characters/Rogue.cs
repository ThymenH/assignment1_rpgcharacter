using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public class Rogue : Character
    {
        /// <summary>
        /// Creates roque
        /// </summary>
        public void CreateRogue()
        {
            Rogue baseLevel = new Rogue()
            {
                Name = "Jack",
                Level = 1,
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1,
                CharacterType = CharacterType.CHARACTER_ROGUE
            };

            CharacterBase(baseLevel.Name, baseLevel.Level, baseLevel.Strength, baseLevel.Dexterity, baseLevel.Intelligence, baseLevel.CharacterType);

            AllowedWeapons.Add(WeaponType.WEAPON_DAGGER);
            AllowedWeapons.Add(WeaponType.WEAPON_SWORD);
            AllowedArmor.Add(ArmorType.ARMOR_MAIL);
            AllowedArmor.Add(ArmorType.ARMOR_LEATHER);

        }

        /// <summary>
        /// Levels up roque
        /// </summary>
        public void LevelUpRogue()
        {
            Rogue levelUpRogue = new Rogue()
            {
                Strength = Strength + 1,
                Dexterity = Dexterity + 4,
                Intelligence = Intelligence + 1
            };

            CurrentStats(levelUpRogue.Strength, levelUpRogue.Dexterity, levelUpRogue.Intelligence);

            levelUp();
        }
    }
}
