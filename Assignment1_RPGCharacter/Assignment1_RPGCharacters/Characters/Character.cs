using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public abstract class Character : PrimaryAttributes
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public double Damage { get; set; }
        public int StrengthWithArmor { get; set; }
        public int DexterityWithArmor { get; set; }
        public int IntelligenceWithArmor { get; set; }
        public CharacterType CharacterType { get; set; }
        public Inventory Inventory { get; set; }
        public List<WeaponType> AllowedWeapons { get; set; }
        public List<ArmorType> AllowedArmor { get; set; }


        /// <summary>
        /// Adds character information to properties.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        /// <param name="characterType"></param>
        public void CharacterBase(string name, int level, int strength, int dexterity, int intelligence, CharacterType characterType)
        {
            Name = name;
            Level = level;
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
            CharacterType = characterType;
            Inventory = new Inventory();
            AllowedWeapons = new List<WeaponType>();
            AllowedArmor = new List<ArmorType>();
            Console.WriteLine($"A Character called {Name} has been created.");
        }
        /// <summary>
        /// Updates stats after level up.
        /// </summary>
        /// <param name="strength"></param>
        /// <param name="dexterity"></param>
        /// <param name="intelligence"></param>
        public void CurrentStats(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }
        /// <summary>
        /// Adds level to Level property.
        /// </summary>
        public void levelUp()
        {
            Level += 1;
            Console.WriteLine("Level has increased by 1");
        }
        /// <summary>
        /// Creates a StringBuilder of the characters current stats.
        /// </summary>
        /// <param name="strengthWithArmor"></param>
        /// <param name="dexterityWithArmor"></param>
        /// <param name="intelligenceWithArmor"></param>
        public void CheckStats()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine($"Character name: {Name}");
            sb.AppendLine($"Character Level: {Level}");
            sb.AppendLine($"Strength: {StrengthWithArmor}");
            sb.AppendLine($"Dexterity: {DexterityWithArmor}");
            sb.AppendLine($"Intelligence: {IntelligenceWithArmor}");
            sb.AppendLine($"Damage: {Damage}");
            Console.WriteLine(sb);
        }
        /// <summary>
        /// Adds a weapon to the characters weapon slot.
        /// </summary>
        /// <param name="slot"></param>
        /// <param name="weaponType"></param>
        /// <param name="item"></param>
        /// <exception cref="InvalidWeaponException">Wrong weapon type or weapon level too high</exception>
        /// <returns>A message when a weapon was successfully added to the weapon slot</returns>
        public string EquipItem(InventorySlots slot, WeaponType weaponType, Weapon item)
        {
            // Check if item level is higher than current level
            if(item.ItemLevel <= Level)
            {
                // Check if item is allowed for this character
                if (AllowedWeapons.Contains(weaponType))
                {
                    // Check if Weapon slot is taken
                    if (Inventory.WeaponContents.ContainsKey(slot))
                    {
                        // Remove current Weapon in Weapon slot
                        Inventory.WeaponContents.Remove(slot);
                        Console.WriteLine("Weapon has been replaced!");
                    }
                    // Add new Weapon to Weapon slot
                    Inventory.WeaponContents.Add(slot, item);
                    return "New weapon equipped!";
                }
                else
                {
                    throw new InvalidWeaponException("Cannot equip this weapon!");
                }
            }
            else
            {
                throw new InvalidWeaponException("Item level too high");
            }
        }

        /// <summary>
        /// Adds armor to the characters armor slot.
        /// </summary>
        /// <param name="slot"></param>
        /// <param name="armorType"></param>
        /// <param name="item"></param>
        /// <exception cref="InvalidArmorException">Wrong armor type or armor level too high</exception>
        /// <returns>A message when armor was successfully added to the armor slot</returns>
        public string EquipItem(InventorySlots slot, ArmorType armorType, Armor item)
        {
            if(item.ItemLevel <= Level)
            {
                if (AllowedArmor.Contains(armorType))
                {
                    if (Inventory.ArmorContents.ContainsKey(slot))
                    {
                        Inventory.ArmorContents.Remove(slot);
                        Console.WriteLine("Armor has been replaced!");
                    }
                    Inventory.ArmorContents.Add(slot, item);
                    return "New armor equipped!";
                }
                else
                {
                    throw new InvalidArmorException("Cannot equip this armor!");
                }
            }
            else
            {
                throw new InvalidArmorException("Item level too high!");
            }
        }

        /// <summary>
        /// Finds each armor value and adds it to the current armor stats.
        /// </summary>
        public void AddItemAttributes()
        {
            int armorStrength = 0;
            int armorDexterity = 0;
            int armorIntelligence = 0;

            // Iterates through Armor in Armor slots
            foreach (KeyValuePair<InventorySlots,Armor> allItems in Inventory.ArmorContents)
            {
                armorStrength = armorStrength + allItems.Value.Attributes.Strength;
                armorDexterity = armorDexterity + allItems.Value.Attributes.Dexterity;
                armorIntelligence = armorIntelligence + allItems.Value.Attributes.Intelligence;
            }

            // Adds Armor attributes to current character stats
            StrengthWithArmor = Strength + armorStrength;
            DexterityWithArmor = Dexterity + armorDexterity;
            IntelligenceWithArmor = Intelligence + armorIntelligence;

            // Adds Damage to current character stats
            Damage = Math.Round(CharacterDamage(StrengthWithArmor, DexterityWithArmor, IntelligenceWithArmor), 1);
        }

        /// <summary>
        /// Calculates character damage with current equipment.
        /// </summary>
        /// <param name="strengthWithArmor"></param>
        /// <param name="dexterityWithArmor"></param>
        /// <param name="intelligenceWithArmor"></param>
        /// <returns>Current character damage</returns>
        public double CharacterDamage(double strengthWithArmor, double dexterityWithArmor, double intelligenceWithArmor)
        {
            double attackSpeed = 0;
            double damage = 0;

            // Check if a Weapon is equipped
            if (Inventory.WeaponContents.ContainsKey(0))
            {
                attackSpeed += Inventory.WeaponContents[0].WeaponAttributes.AttackSpeed;
                damage += Inventory.WeaponContents[0].WeaponAttributes.Damage;
            }

            // Calculate DPS
            double DPS = damage * attackSpeed;

            if (DPS == 0)
            {
                DPS += 1;
            }

            // Check if stat boosts current character stats. E.g. Strength for Warrior, Dexterity for Ranger and Rogue, Intelligence for Mage
            if (CharacterType == CharacterType.CHARACTER_WARRIOR)
            {
                // Strength
                return DPS * (1 + strengthWithArmor / 100);
            }
            else if(CharacterType == CharacterType.CHARACTER_RANGER || CharacterType == CharacterType.CHARACTER_ROGUE)
            {
                // Dexterity
                return DPS * (1 + dexterityWithArmor / 100);
            }
            else if(CharacterType == CharacterType.CHARACTER_MAGE)
            {
                // Intelligence
                return DPS * (1 + intelligenceWithArmor / 100);
            }
            return 0;
        }
    }
}
