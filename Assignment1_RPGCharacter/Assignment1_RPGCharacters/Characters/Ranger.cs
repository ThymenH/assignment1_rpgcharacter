using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public class Ranger : Character
    {
        /// <summary>
        /// Creates ranger
        /// </summary>
        public void CreateRanger()
        {
            Ranger baseLevel = new Ranger()
            {
                Name = "Jared",
                Level = 1,
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1,
                CharacterType = CharacterType.CHARACTER_RANGER
            };

            CharacterBase(baseLevel.Name, baseLevel.Level, baseLevel.Strength, baseLevel.Dexterity, baseLevel.Intelligence, baseLevel.CharacterType);

            AllowedWeapons.Add(WeaponType.WEAPON_BOW);
            AllowedArmor.Add(ArmorType.ARMOR_MAIL);
            AllowedArmor.Add(ArmorType.ARMOR_LEATHER);
        }

        /// <summary>
        /// Levels up ranger
        /// </summary>
        public void LevelUpRanger()
        {
            Ranger levelUpRanger = new Ranger()
            {
                Strength = Strength + 1,
                Dexterity = Dexterity + 5,
                Intelligence = Intelligence + 1
            };

            CurrentStats(levelUpRanger.Strength, levelUpRanger.Dexterity, levelUpRanger.Intelligence);

            levelUp();
        }
    }
}
