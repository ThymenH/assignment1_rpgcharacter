﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1_RPGCharacters
{
    public class WeaponAttributes
    {
        public int Damage { get; set; }
        public double AttackSpeed { get; set; }
    }
}
