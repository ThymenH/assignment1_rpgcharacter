﻿using System;

namespace Assignment1_RPGCharacters
{
    class Program
    {
        static void Main(string[] args)
        {
            // Warrior test case
            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();
            Weapon coolAxe = new Weapon()
            {
                ItemName = "Cool axe",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Weapon newCoolAxe = new Weapon()
            {
                ItemName = "New Cool axe",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 10, AttackSpeed = 1.4 }
            };
            Armor coolPlateBody = new Armor()
            {
                ItemName = "Cool plate body armor",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_BODY,
                TypeArmor = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };
            Armor newCoolPlateBody = new Armor()
            {
                ItemName = "New Cool plate body armor",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_BODY,
                TypeArmor = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Strength = 2 }
            };
            WarriorCharacter.LevelUpWarrior();

                // Equip weapon
            WarriorCharacter.EquipItem(coolAxe.ItemSlot, coolAxe.TypeWeapon, coolAxe);

                // Replace old weapon and equip new weapon
            WarriorCharacter.EquipItem(newCoolAxe.ItemSlot, newCoolAxe.TypeWeapon, newCoolAxe);

                // Equip body armor
            WarriorCharacter.EquipItem(coolPlateBody.ItemSlot, coolPlateBody.TypeArmor, coolPlateBody);

                // Replace old body armor and equip new body armor
            WarriorCharacter.EquipItem(newCoolPlateBody.ItemSlot, newCoolPlateBody.TypeArmor, newCoolPlateBody);

            WarriorCharacter.AddItemAttributes();
            WarriorCharacter.CheckStats();

            // Rogue test case
            Rogue RogueCharacter = new Rogue();
            RogueCharacter.CreateRogue();
            Weapon pointyDagger = new Weapon()
            {
                ItemName = "Pointy Dagger",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_DAGGER,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor cleanMailBody = new Armor()
            {
                ItemName = "Clean mail body armor",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_BODY,
                TypeArmor = ArmorType.ARMOR_MAIL,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };
            RogueCharacter.LevelUpRogue();
            RogueCharacter.EquipItem(pointyDagger.ItemSlot, pointyDagger.TypeWeapon, pointyDagger);
            RogueCharacter.EquipItem(cleanMailBody.ItemSlot, cleanMailBody.TypeArmor, cleanMailBody);
            RogueCharacter.AddItemAttributes();
            RogueCharacter.CheckStats();

            // Ranger test case
            Ranger RangerCharacter = new Ranger();
            RangerCharacter.CreateRanger();
            Weapon fastBow = new Weapon()
            {
                ItemName = "Fast Bow",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor slickLeatherPants = new Armor()
            {
                ItemName = "Slick Leather Leg armor",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_LEGS,
                TypeArmor = ArmorType.ARMOR_LEATHER,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };
            RangerCharacter.LevelUpRanger();
            RangerCharacter.EquipItem(fastBow.ItemSlot, fastBow.TypeWeapon, fastBow);
            RangerCharacter.EquipItem(slickLeatherPants.ItemSlot, slickLeatherPants.TypeArmor, slickLeatherPants);
            RangerCharacter.AddItemAttributes();
            RangerCharacter.CheckStats();

            // Mage test case
            Mage MageCharacter = new Mage();
            MageCharacter.CreateMage();
            Weapon greatStaff = new Weapon()
            {
                ItemName = "Great Staff",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_STAFF,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };
            Armor wornClothHelmet = new Armor()
            {
                ItemName = "Slick Leather Leg armor",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_HEAD,
                TypeArmor = ArmorType.ARMOR_CLOTH,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };
            MageCharacter.LevelUpMage();
            MageCharacter.EquipItem(greatStaff.ItemSlot, greatStaff.TypeWeapon, greatStaff);
            MageCharacter.EquipItem(wornClothHelmet.ItemSlot, wornClothHelmet.TypeArmor, wornClothHelmet);
            MageCharacter.AddItemAttributes();
            MageCharacter.CheckStats();
        }
    }
}
