﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assignment1_RPGCharacters;

namespace Assignment1_RPGCharacters_Tests
{
    public class CharacterTests
    {
        // 1) A character is level 1 when created

        [Fact]
        public void CreateWarrior_CheckLevelWhenCreated_ReturnsLevelAfterCreation()
        {
            // Arrange
            int expected = 1;
            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            // Act
            int actual = WarriorCharacter.Level;


            // Assert
            Assert.Equal(expected, actual);
        }

        // 2) When a character gains a level, it should be level 2

        [Fact]
        public void LevelUpWarrior_CheckLevelAfterLevelUp_ReturnsLevelAfterLevelUp()
        {
            // Arrange
            int expected = 2;
            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();
            WarriorCharacter.LevelUpWarrior();

            // Act
            int actual = WarriorCharacter.Level;


            // Assert
            Assert.Equal(expected, actual);
        }

        // 3) Each character class is created with the proper default attributes
            // Use level 1 stats for each character as expected
            // This results in four test methods

        [Fact]
        public void CreateWarrior_CheckAttributesOnCreation_ReturnsAllAttributesAfterCreation()
        {
            // Arrange
            int[] expected = new int[] { 5,2,1 };
            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            // Act
            int[] actual = new int[] { WarriorCharacter.Strength, WarriorCharacter.Dexterity, WarriorCharacter.Intelligence };

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateRogue_CheckAttributesOnCreation_ReturnsAllAttributesAfterCreation()
        {
            // Arrange
            int[] expected = new int[] { 2, 6, 1 };
            Rogue RogueCharacter = new Rogue();
            RogueCharacter.CreateRogue();

            // Act
            int[] actual = new int[] { RogueCharacter.Strength, RogueCharacter.Dexterity, RogueCharacter.Intelligence };

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateRanger_CheckAttributesOnCreation_ReturnsAllAttributesAfterCreation()
        {
            // Arrange
            int[] expected = new int[] { 1, 7, 1 };
            Ranger RangerCharacter = new Ranger();
            RangerCharacter.CreateRanger();

            // Act
            int[] actual = new int[] { RangerCharacter.Strength, RangerCharacter.Dexterity, RangerCharacter.Intelligence };

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateMage_CheckAttributesOnCreation_ReturnsAllAttributesAfterCreation()
        {
            // Arrange
            int[] expected = new int[] { 1, 1, 8 };
            Mage MageCharacter = new Mage();
            MageCharacter.CreateMage();

            // Act
            int[] actual = new int[] { MageCharacter.Strength, MageCharacter.Dexterity, MageCharacter.Intelligence };

            // Assert
            Assert.Equal(expected, actual);
        }

        // 4) Each character class has their attributes increased when leveling up
            // Create each class once, level them up once
            // Use the base attributes, plus one instance of the level up as the expected
            // E.g. Warrior -> levelUp() -> (Strength = 8, Dexterity = 4, Intelligence = 2) expected
            // This results in four test methods

        [Fact]
        public void LevelUpWarrior_CheckAttributesAfterLevelUp_ReturnsAllAttributesAfterLevelUp()
        {
            // Arrange
            int[] expected = new int[] { 8, 4, 2 };
            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();
            WarriorCharacter.LevelUpWarrior();

            // Act
            int[] actual = new int[] { WarriorCharacter.Strength, WarriorCharacter.Dexterity, WarriorCharacter.Intelligence };

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpRogue_CheckAttributesAfterLevelUp_ReturnsAllAttributesAfterLevelUp()
        {
            // Arrange
            int[] expected = new int[] { 3, 10, 2 };
            Rogue RogueCharacter = new Rogue();
            RogueCharacter.CreateRogue();
            RogueCharacter.LevelUpRogue();

            // Act
            int[] actual = new int[] { RogueCharacter.Strength, RogueCharacter.Dexterity, RogueCharacter.Intelligence };

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpRanger_CheckAttributesAfterLevelUp_ReturnsAllAttributesAfterLevelUp()
        {
            // Arrange
            int[] expected = new int[] { 2, 12, 2 };
            Ranger RangerCharacter = new Ranger();
            RangerCharacter.CreateRanger();
            RangerCharacter.LevelUpRanger();

            // Act
            int[] actual = new int[] { RangerCharacter.Strength, RangerCharacter.Dexterity, RangerCharacter.Intelligence };

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUpMage_CheckAttributesAfterLevelUp_ReturnsAllAttributesAfterLevelUp()
        {
            // Arrange
            int[] expected = new int[] { 2, 2, 13 };
            Mage MageCharacter = new Mage();
            MageCharacter.CreateMage();
            MageCharacter.LevelUpMage();

            // Act
            int[] actual = new int[] { MageCharacter.Strength, MageCharacter.Dexterity, MageCharacter.Intelligence };

            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
