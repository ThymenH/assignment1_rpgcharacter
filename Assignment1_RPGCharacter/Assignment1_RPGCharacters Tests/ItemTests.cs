﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Assignment1_RPGCharacters;

namespace Assignment1_RPGCharacters_Tests
{
    public class ItemTests
    {
        // 1) If a character tries to equip a high level weapon, InvalidWeaponException should be thrown.
            // Use the warrior, and the axe, but set the axes level to 2.

        [Fact]
        public void EquipItem_EquipWeaponWithTooHighLevel_ShouldReturnInvalidWeaponException()
        {
            // Arrange
            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 2,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => WarriorCharacter.EquipItem(testAxe.ItemSlot, testAxe.TypeWeapon, testAxe));
        }

        // 2) If a character tries to equip a high level armor piece, InvalidArmorException should be thrown.
            //Use the warrior, and the plate body armor, but set the armor’s level to 2

        [Fact]
        public void EquipItem_EquipArmorWithTooHighLevel_ShouldReturnInvalidArmorException()
        {
            // Arrange
            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 2,
                ItemSlot = InventorySlots.SLOT_BODY,
                TypeArmor = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => WarriorCharacter.EquipItem(testPlateBody.ItemSlot, testPlateBody.TypeArmor, testPlateBody));
        }

        // 3) If a character tries to equip the wrong weapon type, InvalidWeaponException should be thrown.
            // Use the warrior and the bow

        [Fact]
        public void EquipItem_EquipWrongWeaponType_ShouldReturnInvalidWeaponException()
        {
            // Arrange
            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            Weapon testBow = new Weapon()
            {
                ItemName = "Common bow",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_BOW,
                WeaponAttributes = new WeaponAttributes() { Damage = 12, AttackSpeed = 0.8 }
            };

            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => WarriorCharacter.EquipItem(testBow.ItemSlot, testBow.TypeWeapon, testBow));
        }

        // 4) If a character tries to equip the wrong armor type, InvalidArmorException should be thrown.
            // Use the warrior and the cloth armor.
        

        [Fact]
        public void EquipItem_EquipWrongArmorType_ShouldReturnInvalidArmorException()
        {
            // Arrange
            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            Armor testClothHead = new Armor()
            {
                ItemName = "Common cloth head armor",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_HEAD,
                TypeArmor = ArmorType.ARMOR_CLOTH,
                Attributes = new PrimaryAttributes() { Intelligence = 5 }
            };

            // Act & Assert
            Assert.Throws<InvalidArmorException>(() => WarriorCharacter.EquipItem(testClothHead.ItemSlot, testClothHead.TypeArmor, testClothHead));
        }

        // 5) If a character equips a valid weapon, a success message should be returned
            // “New weapon equipped!”

        [Fact]
        public void EquipItem_EquipValidWeapon_ShouldReturnValidMessage()
        {
            // Arrange
            string expected = "New weapon equipped!";

            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            // Act
            string actual = WarriorCharacter.EquipItem(testAxe.ItemSlot, testAxe.TypeWeapon, testAxe);


            // Assert
            Assert.Equal(expected, actual);
        }

        // 5) If a character equips a valid weapon, a success message should be returned
            // “New weapon equipped!”

        [Fact]
        public void EquipItem_EquipValidArmor_ShouldReturnValidMessage()
        {
            // Arrange
            string expected = "New armor equipped!";

            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_BODY,
                TypeArmor = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };

            // Act
            string actual = WarriorCharacter.EquipItem(testPlateBody.ItemSlot, testPlateBody.TypeArmor, testPlateBody);


            // Assert
            Assert.Equal(expected, actual);
        }

        // 7) Calculate Damage if no weapon is equipped.
            // Take warrior at level 1
            // Expected Damage = 1*(1 + (5 / 100))

        [Fact]
        public void AddItemAttributes_CalculateDamageWithoutWeapon_ShouldReturnCurrentDamage()
        {
            // Arrange
            double expected = 1;

            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();
            WarriorCharacter.AddItemAttributes();

            // Act
            double actual = WarriorCharacter.Damage;


            // Assert
            Assert.Equal(expected, actual);
        }

        // 8) Calculate Damage with valid weapon equipped.
            // Take warrior level 1.
            //Equip axe.
            //Expected Damage = (7 * 1.1)*(1 + (5 / 100))

        [Fact]
        public void AddItemAttributes_CalculateDamageWithValidWeapon_ShouldReturnCurrentDamage()
        {
            // Arrange
            double expected = Math.Round((7.0 * 1.1) * (1.0 + (5.0 / 100.0)), 1); // 8,1

            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            WarriorCharacter.EquipItem(testAxe.ItemSlot, testAxe.TypeWeapon, testAxe);

            WarriorCharacter.AddItemAttributes();

            // Act
            double actual = WarriorCharacter.Damage;


            // Assert
            Assert.Equal(expected, actual);
        }

        // 9) Calculate Damage with valid weapon and armor equipped.
            // Take warrior level 1.
            // Equip axe.
            // Equip plate body armor.
            // Expected Damage = (7 * 1.1) * (1 + ((5+1) / 100))

        [Fact]
        public void AddItemAttributes_CalculateDamageWithValidWeaponAndValidArmor_ShouldReturnCurrentDamage()
        {
            // Arrange
            double expected = Math.Round((7.0 * 1.1) * (1.0 + ((5.0 + 1.0) / 100.0)), 1); // 8,2

            Warrior WarriorCharacter = new Warrior();
            WarriorCharacter.CreateWarrior();

            Weapon testAxe = new Weapon()
            {
                ItemName = "Common axe",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_WEAPON,
                TypeWeapon = WeaponType.WEAPON_AXE,
                WeaponAttributes = new WeaponAttributes() { Damage = 7, AttackSpeed = 1.1 }
            };

            Armor testPlateBody = new Armor()
            {
                ItemName = "Common plate body armor",
                ItemLevel = 1,
                ItemSlot = InventorySlots.SLOT_BODY,
                TypeArmor = ArmorType.ARMOR_PLATE,
                Attributes = new PrimaryAttributes() { Strength = 1 }
            };

            WarriorCharacter.EquipItem(testAxe.ItemSlot, testAxe.TypeWeapon, testAxe);
            WarriorCharacter.EquipItem(testPlateBody.ItemSlot, testPlateBody.TypeArmor, testPlateBody);

            WarriorCharacter.AddItemAttributes();

            // Act
            double actual = WarriorCharacter.Damage;


            // Assert
            Assert.Equal(expected, actual);
        }
    }
}
